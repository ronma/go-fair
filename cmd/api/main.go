// Copyright 2017 Emir Ribic. All rights reserved.
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file.

// GO-FAIR
//
// API Docs v1
//
// 	 Terms Of Service:  N/A
//     Schemes: http
//     Version: 1.0.0
//     Host: localhost:8080
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - bearer: []
//
//     SecurityDefinitions:
//     bearer:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta
package main

import (
	"github.com/labstack/echo"
	"bitbucket.org/ronma/go-fair/internal/platform/postgres"

	"github.com/go-pg/pg"
	"bitbucket.org/ronma/go-fair/cmd/api/config"
	"bitbucket.org/ronma/go-fair/cmd/api/mw"
	"bitbucket.org/ronma/go-fair/cmd/api/server"
	"bitbucket.org/ronma/go-fair/cmd/api/service"
	_ "bitbucket.org/ronma/go-fair/cmd/api/swagger"
	"bitbucket.org/ronma/go-fair/internal/account"
	"bitbucket.org/ronma/go-fair/internal/auth"
	"bitbucket.org/ronma/go-fair/internal/rbac"
)

func main() {

	cfg, err := config.Load("dev")
	checkErr(err)

	e := server.New()

	db, err := pgsql.New(cfg.DB)
	checkErr(err)

	addV1Services(cfg, e, db)

	server.Start(e, cfg.Server)
}

func addV1Services(cfg *config.Configuration, e *echo.Echo, db *pg.DB) {

	// Initialize DB interfaces

	userDB := pgsql.NewUserDB(e.Logger)
	accDB := pgsql.NewAccountDB(e.Logger)

	// Initialize services

	jwt := mw.NewJWT(cfg.JWT)
	authSvc := auth.New(db, userDB, jwt)
	service.NewAuth(authSvc, e, jwt.MWFunc())

	e.Static("/swaggerui", "cmd/api/swaggerui")

	rbacSvc := rbac.New(userDB)

	v1Router := e.Group("/v1")

	v1Router.Use(jwt.MWFunc())

	// Workaround for Echo's issue with routing.
	// v1Router should be passed to service normally, and then the group name created there
	service.NewAccount(account.New(db, accDB, userDB, rbacSvc), v1Router)
}

func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}
