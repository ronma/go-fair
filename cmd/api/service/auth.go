package service

import (
	"net/http"

	"github.com/labstack/echo"
	"bitbucket.org/ronma/go-fair/cmd/api/request"

	"bitbucket.org/ronma/go-fair/internal/auth"
)

// Auth represents auth http service
type Auth struct {
	svc *auth.Service
}

// NewAuth creates new auth http service
func NewAuth(svc *auth.Service, e *echo.Echo, mw echo.MiddlewareFunc) {
	a := Auth{svc}
	// swagger:route POST /v1/login auth login
	// Login a new user.
	// responses:
	//  200: loginResp
	//  400: errMsg
	//  401: errMsg
	// 	403: err
	//  404: errMsg
	//  500: err
	e.POST("/login", a.login)
	// swagger:operation GET /v1/refresh/{token} auth refresh
	// ---
	// summary: Refreshes jwt token.
	// description: Refreshes jwt token by checking at database whether refresh token exists.
	// parameters:
	// - name: token
	//   in: path
	//   description: refresh token
	//   type: string
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/refreshResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	e.GET("/refresh/:token", a.refresh)

	// swagger:route GET /v1/secret user meReq
	// Gets user's info from session.
	// responses:
	//  200: userResp
	//  500: err
	e.GET("/secret", a.me, mw)
}

func (a *Auth) login(c echo.Context) error {
	cred, err := request.Login(c)
	if err != nil {
		return err
	}
	r, err := a.svc.Authenticate(c, cred.Username, cred.Password)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, r)
}

func (a *Auth) refresh(c echo.Context) error {
	r, err := a.svc.Refresh(c, c.Param("token"))
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, r)
}

func (a *Auth) me(c echo.Context) error {
	user, err := a.svc.Me(c)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, user)
}
