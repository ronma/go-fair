package service

import (
	"net/http"

	"github.com/labstack/echo"
	"bitbucket.org/ronma/go-fair/internal"

	"bitbucket.org/ronma/go-fair/internal/account"

	"bitbucket.org/ronma/go-fair/cmd/api/request"
)

// Account represents account http
type Account struct {
	svc *account.Service
}

// NewAccount creates new account http service
func NewAccount(svc *account.Service, ar *echo.Group) {
	a := Account{svc: svc}
	// swagger:route POST /v1/user auth accCreate
	// Signup a new user and login.
	// responses:
	//  200: userResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	ar.POST("", a.create)
}

func (a *Account) create(c echo.Context) error {
	r, err := request.AccountCreate(c)
	if err != nil {
		return err
	}
	usr, err := a.svc.Create(c, model.User{
		Username:   r.Username,
		Password:   r.Password,
		Email:      r.Email,
		FirstName:  r.FirstName,
		LastName:   r.LastName,
		CompanyID:  r.CompanyID,
		LocationID: r.LocationID,
		RoleID:     r.RoleID,
	})
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, usr)
}

func (a *Account) changePassword(c echo.Context) error {
	p, err := request.PasswordChange(c)
	if err != nil {
		return err
	}
	if err := a.svc.ChangePassword(c, p.OldPassword, p.NewPassword, p.ID); err != nil {
		return err
	}
	return c.NoContent(http.StatusOK)
}
