.DEFAULT_GOAL: generate-swagger build

.PHONY: build
build:
	cd cmd/migration && go get && go build
	cd cmd/api && go get && go build

.PHONY: generate-swagger
generate-swagger:
	cd cmd/api && go get -u github.com/go-swagger/go-swagger/cmd/swagger && \
		swagger generate spec -o swaggerui/swagger.json --scan-models

.PHONY: start-dev
start-dev:
	docker-compose up -d
	docker-compose ps
	sleep 2 && go run cmd/migration/main.go || true
	ENVIRONMENT_NAME=dev go run cmd/api/main.go

.PHONY: stop-dev
stop-dev:
	docker-compose stop
	docker-compose rm -f
	docker-compose ps

.PHONY: debug-dev
debug-dev:
	go get github.com/derekparker/delve/cmd/dlv
	echo "\033[0;32m\nTo debug in VSCODE: Switch to Debug tab (Shift-Cmd-D), and launch 'debug-dev' configuration\n\033[0m" && \
	cd cmd/api && ENVIRONMENT_NAME=dev dlv .debug-dev --headless --listen=:2345 --api-version=1
